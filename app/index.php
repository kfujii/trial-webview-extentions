<?php

namespace FrontController;

require __DIR__ . '/../vendor/autoload.php';

use Slim\App;
use Slim\Container;
use Facebook\Facebook;
use Slim\Views\Twig as View;
use Slim\Views\TwigExtension;
use Doctrine\Common\Cache\FilesystemCache;
use Psr\Http\Message\ServerRequestInterface as Req;
use Psr\Http\Message\ResponseInterface as Res;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

date_default_timezone_set('Asia/Tokyo');

$settings = [
    'displayErrorDetails' => true,
    'facebook' => [
        'app_id' => getenv('FB_APP_ID'),
        'app_secret' => getenv('FB_APP_SECRET'),
        'default_graph_version' => 'v2.8',
        'http_client_handler' => 'curl',
    ]
];

$app = new App([
    'settings' => $settings
]);
$container = $app->getContainer();

// DI
/**
 * facebook
 *
 * @return \Facebook\Facebook
 */
$container['facebook'] = function (Container $c) {
    $facebook = new Facebook(
        $c->settings['facebook']
    );
    $app = $facebook->getApp();
    $facebook->setDefaultAccessToken(
        getenv('PAGE_TOKEN')
    );

    return $facebook;
};

/**
 * view
 *
 * @return \Slim\Views\Twig
 */
$container['view'] = function (Container $c) {
    $view = new View(
        [realpath(__DIR__ . '/../templates')],
        [
            'cache' => realpath(__DIR__ . '/../cache/twig'),
            'debug' => true
        ]
    );

    $basePath = str_ireplace('index.php', '', $c['request']->getUri()->getBasePath());
    $basePath = rtrim($basePath, '/');
    $view->addExtension(
        new TwigExtension($c['router'], $basePath)
    );

    $twig = $view->getEnvironment();

    // debug extention
    $twig->addExtension(new \Twig_Extension_Debug());

    // Requestをセット
    $view['request'] = $c['request'];
    $view['settings'] = $c['settings'];

    return $view;
};

/**
 * cache 
 *
 * @return \Doctrine\Common\Cache\FilesystemCache
 */
$container['cache'] = function() {
    $cacheDir = __DIR__ . '/../cache/fs';
    return new FilesystemCache($cacheDir);
};


/**
 * logger
 */
$container['logger'] = function($container) {
    $logger = new Logger('basic_logger');
    $logger->pushHandler(new StreamHandler('php://stderr', Logger::DEBUG));

    return $logger;
};

// routings
// app toppage
$app->get('/', function(Req $req, Res $res) {
    return $this->view->render(
        $res,
        'index.twig',
        [
            'page_id' => getenv('PAGE_ID'),
            'app_id' => getenv('FB_APP_ID'),
        ]
    );
});

// GET /callback
$app->get('/callback', function(Req $req, Res $res) {
    // TODO: アサーション 環境変数FB_VERIFY_TOKEN
    $token = getenv('FB_VERIFY_TOKEN');
    $verifyToken = $req->getParam('hub_verify_token');
    $challenge = $req->getParam('hub_challenge');

    if ($token === $verifyToken) {
        return $res->write($challenge);
    }

    return $res->withStatus(400)
        ->write('Error');
})->setName('callback');

// POST /callback
$app->post('/callback', function(Req $req, Res $res) {
    $body = $req->getBody();
    $this->logger->addDebug($body);

    $data = json_decode($body, true);

    $uri = $req->getUri();
    $webViewUri = $uri->withPath($this->get('router')
        ->pathFor('webview'))
        ->withScheme('https')
        ->withPort(443);
    $fallbackUri = $uri->withPath($this->get('router')
        ->pathFor('webview-fallback'))
        ->withScheme('https')
        ->withPort(443);

    foreach ($data['entry'] as $entry) {
        $pageId = $entry['id'];
        $this->logger->addInfo($pageId);
        if (!array_key_exists('messaging', $entry)) {
            return $res;
        }
        foreach ($entry['messaging'] as $messaging) {
            $this->logger->addInfo(json_encode($messaging));

            if (array_key_exists('optin', $messaging)) {
                // 返信 optin
                $fb = $this->facebook;
                // ユーザー
                $this->logger->addInfo($messaging['sender']['id']);
                try {
                    $user = $fb->get('/' . $messaging['sender']['id'] . '?locale=ja_JP');
                    $this->logger->addInfo($user->getBody());
                } catch (\Exception $e) {
                    $this->logger->addInfo($e->getMessage());
                }

                $this->logger->addInfo((string)$webViewUri);
                $message = [
                    'recipient' => $messaging['sender'],
                    'message' => [
                        'attachment' => [
                            'type' => 'template',
                            'payload' => [
                                'template_type' => 'generic',
                                'elements' => [
                                    [
                                        'title' => '投票ありがとうありがとう！',
                                        'subtitle' => 'コメントは任意です',
                                        'image_url' => 'https://scontent-nrt1-1.xx.fbcdn.net/v/t1.0-9/16265646_1187934691314319_7841534232650797959_n.jpg?oh=71a4631d664bf26553e4979c4f42fbf7&oe=5947992E',
                                        'buttons' => [
                                            [
                                                'type' => 'web_url',
                                                'url' => (string)$webViewUri->withQuery(http_build_query($user->getDecodedBody())),
                                                'messenger_extensions' => true,
                                                'fallback_url' => (string)$fallbackUri->withQuery(http_build_query($user->getDecodedBody())),
                                                'webview_height_ratio' => 'compact',
                                                'title' => 'コメントする'
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];
                try {
                    $response = $fb->post('/me/messages', $message);
                } catch (\Exception $e) {
                    $this->logger->addInfo('Err');
                    $this->logger->addError($e->getMessage());
                }

                return $res;
            }
        }

    }

    return $res;
});

// GET for webview
$app->get('/webview', function(Req $req, Res $res) {
    return $this->view->render(
        $res,
        'webview/index.twig',
        [
            'query' => $req->getParams()
        ]
    );
})->setName('webview');

$app->get('/webview-pc', function(Req $req, Res $res) {
    return $this->view->render(
        $res,
        'webview/fallback.twig',
        [
            'query' => $req->getParams()
        ]
    );
})->setName('webview-fallback');

// ajax経由でログ吐くよ
$app->post('/ajax', function(Req $req, Res $res) {
    $log = json_encode($_POST);
    $this->logger->addInfo($log);

    return $res;
})->setName('ajax');



$app->run();

# Facebook MessengerプラットフォームでWebviewを利用するための検証コード

## 環境構築

herokuで動かすことを想定しています。

`heroku config:set`で以下の環境変数をセットしておきます。
ほとんどがFacebook上で取得できる情報です。全て必須です。

* FB_APP_ID 
* FB_APP_SECRET
* FB_VERIFY_TOKEN webhook設定時に使うランダムな文字列
* PAGE_ID
* PAGE_TOKEN


